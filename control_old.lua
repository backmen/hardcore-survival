require "util"
require "stdlib/table"
require "stdlib/game"

require "stdlib/area/position"
require 'stdlib/area/area'

require 'stdlib/config/config'
require 'stdlib/event/event'

-- const (change to get desync)
local SpawnEveryXTicks = 600

function SendPrintToAllPlayers(message)
    if game.player ~= nil then
        game.player.print("[hardcore-survival] "..message)
    end
    Game.print_all("[hardcore-survival] "..message)
end

local function checkInit()
    if global.MobsInEvent == nil then
        global.MobsInEvent = {}
    end
    if global.Night == nil then
        global.Night = 0
    end
    if global.isNightTime == nil then -- sync
        global.isNightTime = false
        game.surfaces['nauvis'].daytime = .45
    end
	
    if global.firstRun == nil then
        global.firstRun = true
    end
    if global.UnitsSpawnedThisNight == nil then
        global.UnitsSpawnedThisNight = 0
    end
    if global.CheckNumberUnitsAliveCounter == nil then
        global.CheckNumberUnitsAliveCounter = 0
    end
    if global.TotalNightEnemies == nil then
        global.TotalNightEnemies = 0
    end
    if global.NightForUnitCount == nil then
        global.NightForUnitCount = global.Night
    end
    if global.EnemiesCanReachPlayer == nil then
        global.EnemiesCanReachPlayer = false
    end
	
    if global.maxPollution == nil then
        global.maxPollution = 0
    end
    if global.AttackPos == nil then
        global.AttackPos = {x = 0, y = 0};
    end
    if global.tutorial == nil then
        global.tutorial = false
    end

    if global.bugs == nil then
        global.bugs = {
            { id = 0, type="small-biter", count=0 },
            { id = 1, type="medium-biter", count=0 },
            { id = 2, type="big-biter", count=0 },
            { id = 3, type="behemoth-biter", count=0 },
            { id = 4, type="mega-biter", count=0 }
        }
    end
end

script.on_init(function()
    game.surfaces['nauvis'].daytime = .45
    checkInit()
end)

script.on_configuration_changed(function()
    checkInit()
end)



Event.register(defines.events.on_tick, function(event)
    if global.firstRun == nil or global.firstRun == true then
        global.firstRun = false
        checkInit()
        -- Make more of the map so that I can find spawns
        game.surfaces['nauvis'].request_to_generate_chunks({0,0}, 10)
        SendPrintToAllPlayers("Wellcome!")
        return
    end
	-- basic validate
	if global.tutorial == nil then
		global.tutorial = true
	end
	if global.isNightTime == nil then
		global.isNightTime = false
	end
	if (global.Night or 1) < 5 then
		global.tutorial = false -- for test
	end
    -- Set Nights
    if game.surfaces['nauvis'].daytime >= .5 and game.surfaces['nauvis'].daytime < .7 and global.isNightTime == false then
        -- New Night Begins

        if global.tutorial == true then
            SendPrintToAllPlayers("Early start mode! Prepare for Night 5!")
			global.isNightTime = true
			global.Night = (global.Night or 1)+1
			SendPrintToAllPlayers("Night "..(global.Night or 1))
			if (global.Night or 1) > 4 then
				global.tutorial = false
			end
        else
            -- handle triggers
            SendPrintToAllPlayers("NewNightBegins()")
            NewNightBegins()
        end
        return
    elseif (game.surfaces['nauvis'].daytime >= .7 and global.isNightTime == true ) then
        -- Night Ends
        SendPrintToAllPlayers("Night Over")
        --  game.surfaces['nauvis'].freeze_daytime(false)
        global.isNightTime = false

       return
    end
	if global.tutorial == true then -- disable mod logic before night 5
		return
	end
    if event.tick % SpawnEveryXTicks ~= 0 then -- skip tick
        return
	end
    if global.isNightTime == false then --day
		UpdateEnemyTargetLocationDay(event)
	else -- night
		-- Update all enemies, have their target location go to wherever the player has moved to
        SendPrintToAllPlayers("global.UnitsSpawnedThisNight = "..global.UnitsSpawnedThisNight)
        SendPrintToAllPlayers("global.TotalNightEnemies = "..global.TotalNightEnemies)
		if (global.UnitsSpawnedThisNight or 0) < (global.TotalNightEnemies or 10) then -- add more biters (if need)
		--	local allbiters = game.surfaces['nauvis'].count_entities_filtered{force=game.forces.enemy,type="unit" }
		--	SendPrintToAllPlayers('allbiters = '..allbiters)
		--	if allbiters < (global.TotalNightEnemies or 10)  then
				SendPrintToAllPlayers("Bitter left to send to attack "..(global.TotalNightEnemies or 10).." ");
				FindEnemySpawners()
				return
		--	end
		end
	   
		UpdateEnemyTargetLocationNight(event) -- update path
    end
end)

Event.register(defines.events.on_entity_died, function(event)
    if event.entity.force==game.forces.enemy and event.entity.has_command() then
        game.surfaces['nauvis'].spill_item_stack(event.entity.position,{name="firearm-magazine", count=5})
    end
end)

 function NewNightBegins()
    --game.surfaces['nauvis'].freeze_daytime(true)
	global.isNightTime = true
	
	local HoursInGame = math.floor(game.tick / 216000); --60 ticks per sec 
	
	global.Night = (global.Night or 1)+1
       
       -- global.NightForUnitCount resets back to 10 enemies when big units come out
      --global.NightForUnitCount = global.NightForUnitCount + 1
      SendPrintToAllPlayers("Night "..(global.Night or 1))
	  SendPrintToAllPlayers(#game.players.." players")

	  local plcount = math.max(#table.filter(game.players,function(v) return v.connected end),1)
	  
	  local bugs = (global.bugs or {})
     table.each(bugs,function(value)
         value.count = 0 
         if (global.Night or 1) <= 10 and value.id == 0 then
             value.count = 10 + (global.Night or 1) * plcount;
         elseif value.id == 0 then
             value.count = math.min(600,math.floor(game.evolution_factor/0.1) * HoursInGame * plcount +  (global.Night or 1))
         elseif  value.id == 1 then
             value.count = math.min(600,math.floor(game.evolution_factor/0.3) * HoursInGame * plcount +  (global.Night or 1) * math.floor(game.evolution_factor/0.3))
         elseif  value.id == 2 then
             value.count = math.min(500,math.floor(game.evolution_factor/0.4) * HoursInGame * plcount +  (global.Night or 1) * math.floor(game.evolution_factor/0.4))
         elseif  value.id == 3 then
             value.count = math.min(400,math.floor(game.evolution_factor/0.5) * HoursInGame * plcount +  (global.Night or 1) * math.floor(game.evolution_factor/0.5))
         elseif  value.id == 4 then
             value.count = math.min(300,math.floor(game.evolution_factor/0.7) * HoursInGame * plcount +  (global.Night or 1) * math.floor(game.evolution_factor/0.7))
         end
     end)
	 global.bugs = bugs

	-- Spawn a new unit every x ticks during nighttime
     global.TotalNightEnemies = 0
     local att_text = "Attackers list:"
     table.each((global.bugs or {}),function(value)
        global.TotalNightEnemies = ((global.TotalNightEnemies or 0) + value.count)
         att_text = att_text .. " " .. value.type .. " - ".. value.count .. ", "
     end)
     global.TotalNightEnemies = math.min(math.max(global.TotalNightEnemies,10),100) -- range from 10 to 100
     --att_text = att_text .. " mega boss - 0!"

	 SendPrintToAllPlayers(att_text)
     SendPrintToAllPlayers("global.TotalNightEnemies = "..global.TotalNightEnemies)
      
      -- Speed up spawn rate 
    --  global.SpawnEveryXTicks = math.max(600 - (global.NightForUnitCount*global.UnitSpawnSpeedUpPerNightMultiple*#game.players),60)
      
	  --Find max pollution
	  global.maxPollution =  0
	  local currPollution = 0
	  local playerForce = game.surfaces['nauvis'].find_entities_filtered{force="player"}
	   
       for i,v in pairs(playerForce)  do
		currPollution = game.surfaces['nauvis'].get_pollution(v.position)
		if (currPollution>(global.maxPollution or  0)) then
			global.maxPollution =  currPollution
			global.AttackPos =  v.position
			SendPrintToAllPlayers("Polution "..currPollution.." in coords x ="..v.position.x .. " y= "..v.position.y)
		end
       end

     if global.AttackPos == nil then
         global.AttackPos = {x = 0, y = 0}
     end

     --attach live mobs
     table.each((global.bugs or {}),function(value)
         local mobs = game.surfaces['nauvis'].find_entities_filtered{force="enemy",name=value.type};
         SendPrintToAllPlayers(value.type.." is "..#mobs)
        if #mobs > 0 then
            if #mobs < value.count then
                global.MobsInEvent = table.merge(global.MobsInEvent,mobs)
                global.UnitsSpawnedThisNight = global.UnitsSpawnedThisNight + #mobs
            else
                table.sort(mobs, function(a,b)
                    return Position.distance(global.AttackPos,a.position) < Position.distance(global.AttackPos,b.position)
                end)
                local t = table.move(mobs, 1, value.count, 1, {})
                global.MobsInEvent = table.merge(global.MobsInEvent,t)

                global.UnitsSpawnedThisNight = global.UnitsSpawnedThisNight + #t
            end
        end
     end)
 end

 function UpdateEnemyTargetLocationNight(event)
    if (global.AttackPos or nil) == nil then
      SendPrintToAllPlayers("No target to attack! Turn on tutorial!")
      global.tutorial = true
      return
    end

    --game.surfaces['nauvis'].set_multi_command{command={type=defines.command.attack_area,destination=game.players[1].position,radius=20.0},unit_count=5,force=game.forces.enemy};

    if global.MobsInEvent == nil then
        global.MobsInEvent = {}
    end

     SendPrintToAllPlayers("global.MobsInEvent: "..#global.MobsInEvent)

    table.map(global.MobsInEvent,function(bitter)
        if not bitter.valid then
            return
        end

        SendPrintToAllPlayers("bitter: distance "..Position.distance(bitter.position,global.AttackPos))


    --    if not bitter.has_command() then
        bitter.set_command({type=defines.command.attack_area,destination=(global.AttackPos or nil),radius=10.0});
    --    end
    end)
 --[[

        local allbiters = game.su`rfaces['nauvis'].find_entities_filtered{force=game.forces.enemy,type="unit"}
        for i,v in pairs(allbiters)  do

        --  local totalplayers = #game.players
        --  local playertotargetindex = mod(i,totalplayers)+1
          -- Take turns, have units target each player
          v.set_command({type=defines.command.attack_area,destination=(global.AttackPos or nil),radius=10.0});
		  --v.set_command({type=defines.command.attack_area,destination=game.players[playertotargetindex].position,radius=100.0});
		  --{type=defines.command.go_to_location, destination=game.players[playertotargetindex].position})
		  --/c game.players[1].print(game.players[2].position.x)
       end
       ]]
  end

function FindEnemySpawners()

    -- look for the closest enemy unit spawner, that is where new units will come from
    local searcharea = 10
    local enemyunitspawner = game.surfaces['nauvis'].find_entities_filtered{area = {{-1*searcharea, -1*searcharea}, {searcharea, searcharea}}, type= "unit-spawner"}
    while next(enemyunitspawner)  == nil and searcharea < 2000 do
        searcharea = searcharea * 2
        enemyunitspawner = game.surfaces['nauvis'].find_entities_filtered{area = {{-1*searcharea, -1*searcharea}, {searcharea, searcharea}}, type= "unit-spawner"}

    end
    SendPrintToAllPlayers("base found within " .. searcharea .. " units")
	
    table.each((global.bugs or {}),function(value)
            --local numbertospawn = db.NightForUnitCount <= value.count and db.NightForUnitCount or db.NightForUnitCount-value.count
            --value.count = math.max(0,value.count - db.NightForUnitCount)
			SendPrintToAllPlayers("Create bitter id = "..value.id.." value = "..value.type.." count ="..value.count
            .. "distance: "..Position.distance(enemyunitspawner[1].position,global.AttackPos))
            spawnEnemies(value.id,value.count,value.type,enemyunitspawner[1].position)
			
            --SendPrintToAllPlayers("Bitters spawned: "..value.type.." ".. value.count);
    end)
end

function spawnEnemies(id,numbertospawn,unitType,enemyposition)
    if (numbertospawn == 0) then
        return
    end
    local counter = 0

    --SendPrintToAllPlayers("spawnEnemies("..id..","..numbertospawn..",table)")

    while counter < numbertospawn do


        enemyposition.x = enemyposition.x + 10 + counter


        local canplace = game.surfaces['nauvis'].can_place_entity{name=unitType,position=enemyposition}
        while (canplace == false) do
            enemyposition.x = enemyposition.x + 1
            canplace = game.surfaces['nauvis'].can_place_entity{name=unitType,position=enemyposition}
        end

        -- Spawn an enemy at closest unit spawner
        local thisbiter = game.surfaces['nauvis'].create_entity{name=unitType,position=enemyposition,force=game.forces.enemy, target=(global.AttackPos or nil)}

        -- send to pollution
        thisbiter.set_command({type=defines.command.attack_area,destination=(global.AttackPos or nil),radius=10.0})

        if global.MobsInEvent == nil then
            global.MobsInEvent = {}
        end

        table.insert(global.MobsInEvent,thisbiter)


		global.UnitsSpawnedThisNight = (global.UnitsSpawnedThisNight or 1)+1
		
        counter = counter + 1
    end
	SendPrintToAllPlayers("Sending bitter id = "..id.." type = "..unitType.." count ="..numbertospawn);
end

function UpdateEnemyTargetLocationDay(event)
	 -- Update all enemies, have their target location go to wherever the player has moved to
	 if (global.AttackPos or nil) == nil then
		  SendPrintToAllPlayers("No target to attack! Turn on tutorial!")
		  global.tutorial = true
		  return
	  end
	
	--game.surfaces['nauvis'].set_multi_command{command={type=defines.command.attack_area,destination=game.players[1].position,radius=20.0},unit_count=5,force=game.forces.enemy};

    table.map(global.MobsInEvent or {},function(bitter)
        if bitter ~= nil and bitter.valid then
            bitter.destroy()
        end
    end)
    global.MobsInEvent = {}
    --[[
	table.each(game.surfaces['nauvis'].find_entities_filtered{force=game.forces.enemy,type="unit"},function(v)
	    local d = Entity.get_data(v) or {}
	    if d.type ~= nil then
            v.destroy()
        end
	end)
	]]
 end


function mod(a, b)
return a - (math.floor(a/b))
end

--[[

function KillAllEnemyUnits()

local allbiters = game.surfaces['nauvis'].find_entities_filtered{force=game.forces.enemy,type="unit"}
      for i,v in pairs(allbiters)  do
        v.destroy()
      end

end
]]
 
 
 